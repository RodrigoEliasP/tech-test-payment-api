using PaymentApi.Controllers;
using PaymentApi.Classes;
using PaymentApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace PaymentApiTests
{
    [TestClass]
    public class SaleControllerTests
    {
        private DBContext CreateDBContext()
        {
            DbContextOptionsBuilder < DBContext > dbOptionBuilder = new DbContextOptionsBuilder<DBContext>();
            dbOptionBuilder.UseInMemoryDatabase("PaymentApi");
            return new DBContext(dbOptionBuilder.Options);
        }
        [TestMethod]
        public async Task PostSale_WithValidItems_DoesCreate()
        {
            DBContext context = CreateDBContext();
            SaleController controller = new SaleController(context);
            await PostValidSaleWithController(controller); 
            
            if (context.Sales.Count() != 1) Assert.Fail("Error creating Sale");

            Sale? postedSale = await context.Sales.FindAsync(1);
            Assert.IsNotNull(postedSale);

        }
        [TestMethod]
        public async Task PostSale_WithValidItems_DoesCreateVendor()
        {
            DBContext context = CreateDBContext();
            SaleController controller = new SaleController(context);
            await PostValidSaleWithController(controller);

            if (context.Vendors.Count() != 1) Assert.Fail("Error creating Vemdor");

            Vendor? createdVendor = await context.Vendors.FindAsync(1);
            Assert.IsNotNull(createdVendor);

        }
        [TestMethod]
        public async Task PostSale_WithInvalidItems_DoesThrowsException()
        {
            DBContext context = CreateDBContext();
            SaleController controller = new SaleController(context);
            SaleCreate saleCreate = new SaleCreate
            {

                SoldItems = new List<String>(),
                VendorName = "Rog�rio",
                VendorEmail = "rogerio@gmail.com",
                VendorCPF = "15079695674"
            };

            await Assert.ThrowsExceptionAsync<HttpResponseException>(
                async () => await controller.PostSale(saleCreate)
            );
        }
        [TestMethod]
        public async Task UpdateSaleStatus_UpdateToValidStatuses_DoesUpdate()
        {
            DBContext context = CreateDBContext();
            SaleController controller = new SaleController(context);
            await PostValidSaleWithController(controller);

            
            SaleUpdateStatus updateStatus = new SaleUpdateStatus
            {
                Status = "Cancelada"
            };

            await controller.UpdateSaleStatus(1, updateStatus);

            Sale sale = context.Sales.First();

            Assert.AreEqual("Cancelada", sale.Status);
        }
        [TestMethod]
        public async Task UpdateSaleStatus_UpdateToInvalidStatus_ReturnsBadRequest()
        {
            DBContext context = CreateDBContext();
            SaleController controller = new SaleController(context);
            await PostValidSaleWithController(controller);


            SaleUpdateStatus updateStatus = new SaleUpdateStatus
            {
                Status = "Status Inv�lido"
            };
            var res = await controller.UpdateSaleStatus(1, updateStatus);
            
            Assert.IsTrue(res.Result is BadRequestObjectResult);

        }
        private async Task PostValidSaleWithController(SaleController controller)
        {
            
            SaleCreate saleCreate = new SaleCreate
            {
                SoldItems = new List<String> {
                    "Calcinha",
                    "Samba can��o"
                },
                VendorName = "Rog�rio",
                VendorEmail = "rogerio@gmail.com",
                VendorCPF = "15079695674"
            };

            await controller.PostSale(saleCreate);
        }
    }
}