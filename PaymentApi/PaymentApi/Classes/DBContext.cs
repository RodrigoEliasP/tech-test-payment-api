﻿using Microsoft.EntityFrameworkCore;
using PaymentApi.Models;

namespace PaymentApi.Classes
{
    public class DBContext: DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }
        public DbSet<Sale> Sales { get; set; } = null!;
        public DbSet<Vendor> Vendors { get; set; } = null!;

    }
}
