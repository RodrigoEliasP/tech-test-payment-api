using Microsoft.AspNetCore.Mvc;
using PaymentApi.Models;
using PaymentApi.Classes;


namespace PaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {

        private readonly DBContext _context;
        private readonly Dictionary<String, List<String>> _allowedStatusesChangesBySaleStatus;

        public SaleController(DBContext context)
        {
            _context = context;
            _allowedStatusesChangesBySaleStatus = new Dictionary<String, List<String>>{
                ["Aguardando pagamento"] = new List<String> {
                    "Pagamento Aprovado",
                    "Cancelada",
                },
                ["Pagamento Aprovado"] = new List<String>
                {
                    "Enviado para Transportadora",
                    "Cancelada"
                },
                ["Enviado para Transportadora"] = new List<String>
                {
                    "Entregue"
                }
            };
        }

        [HttpGet]
        public IEnumerable<Sale> Get()
        {
            return _context.Sales.Join(
                _context.Vendors,
                s => s.VendorId,
                v => v.Id,
                (s, v) => new Sale
                {
                    Id = s.Id,
                    SoldItems = s.SoldItems,
                    VendorId = s.VendorId,
                    Vendor = v,
                    DateTime = s.DateTime,
                    Status = s.Status
                }
            ).ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Sale> GetSaleById(int id)
        {
            Sale? sale = _context.Sales.Join(
                _context.Vendors,
                s => s.VendorId,
                v => v.Id,
                (s, v) => new Sale
                {
                    Id = s.Id,
                    SoldItems = s.SoldItems,
                    VendorId = s.VendorId,
                    Vendor = v,
                    DateTime = s.DateTime,
                    Status = s.Status
                }
            ).Where(s => s.Id == id).First();

            if (sale != null)
                return sale;
            else
                return NotFound();
        }
        [HttpPost]
        async public Task<ActionResult<Sale>> PostSale(SaleCreate newSale)
        {
            Vendor? vendor = _context.Vendors.Any(v => v.CPF.Equals(newSale.VendorCPF)) ? 
                _context.Vendors.Where(v => v.CPF.Equals(newSale.VendorCPF))
                                .First() : null;

            if (vendor == null)
            {
                vendor = new Vendor
                {
                    Id = _context.Vendors.Count() + 1,
                    CPF = newSale.VendorCPF,
                    Name = newSale.VendorName,
                    Email = newSale.VendorEmail
                };
                _context.Vendors.Add(vendor);
            }

            Sale sale = new Sale
            {
                Id = _context.Sales.Count() + 1,
                SoldItems = newSale.SoldItems,
                Status = "Aguardando pagamento",
                VendorId = vendor.Id,
                DateTime = DateTime.Now
            };

            _context.Add(sale);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetSaleById),
                new { Id = sale.Id },
                sale
            );
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<Sale>> UpdateSaleStatus(int id, SaleUpdateStatus newStatus)
        {
            Sale? sale = await _context.Sales.FindAsync(id);
            

            if (sale == null) return NotFound();

            List<String> allowedStatuses = _allowedStatusesChangesBySaleStatus[sale.Status];
            if (!allowedStatuses.Contains(newStatus.Status)) return BadRequest("Esse status n�o existe ou � inv�lido para essa venda");
            sale.Status = newStatus.Status;
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetSaleById),
                new { Id = sale.Id },
                sale
            );
        }
    }
}