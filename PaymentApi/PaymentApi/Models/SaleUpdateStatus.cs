﻿namespace PaymentApi.Models
{
    public class SaleUpdateStatus
    {
        public String Status { get; set; }
    }
}
