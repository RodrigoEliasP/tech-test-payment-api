﻿namespace PaymentApi.Models
{
    public class SaleCreate
    {
        public List<String> SoldItems { get; set; }

        public string VendorName { get; set; }

        public string VendorEmail { get; set; }

        public string VendorCPF { get; set; }
    }
}
