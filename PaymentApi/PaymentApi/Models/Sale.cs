using PaymentApi.Classes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace PaymentApi.Models
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [JsonIgnore]
        public string soldItems { get; set; }
        [NotMapped]
        public List<String> SoldItems
        {
            get => soldItems.Split(';').ToList();
            set
            {
                if (value.Count() < 1) throw new HttpResponseException(400, "A Venda deve possuir pelo menos um item");
                soldItems = string.Join(";", value);
            }
        }

        public String Status { get; set; }
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }

        public DateTime DateTime { get; set; }
         
    }
}